package router

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestNewCookie(t *testing.T) {
	user := &User{ID: "id"}
	cookie, err := NewCookie(user, "localhost")
	assert.Nil(t, err)
	assert.NotNil(t, cookie)

	userID, err := VerifyCookie(cookie)
	assert.Nil(t, err)
	assert.Equal(t, "id", userID)
}

func TestVerifyCookie(t *testing.T) {
	tc := []struct {
		cookie *http.Cookie
	}{
		{
			cookie: nil,
		},
		{
			cookie: &http.Cookie{},
		},
		{
			cookie: &http.Cookie{Name: cookieName},
		},
		{
			cookie: &http.Cookie{Name: cookieName, Value: "userID:expires"},
		},
		{
			cookie: &http.Cookie{Name: cookieName, Value: "userID:expires:badSignature"},
		},
	}

	for _, c := range tc {
		userID, err := VerifyCookie(c.cookie)
		assert.NotNil(t, err)
		assert.Equal(t, "", userID)
	}
}
