package router

import (
	"bytes"
	"errors"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewServe_Routes(t *testing.T) {
	db := newMemoryDB()
	initErr := db.Init()
	assert.Nil(t, initErr)

	ses := newMemorySession()
	initErr = ses.Init()
	assert.Nil(t, initErr)

	srv := server{
		domain:  "localhost",
		mux:     http.NewServeMux(),
		storage: db,
		session: ses,
	}

	srv.routes()

	ts := httptest.NewUnstartedServer(srv.mux)
	ts.Start()
	defer ts.Close()

	// public pages

	// login
	resp, err := ts.Client().Get(ts.URL)
	assert.Nil(t, err)
	assert.NotNil(t, resp)

	body, err := ioutil.ReadAll(resp.Body)
	assert.Nil(t, err)
	assert.Contains(t, string(body), "<title>Login</title>")

	// signUp
	resp, err = ts.Client().Get(ts.URL + SignUp)
	assert.Nil(t, err)
	assert.NotNil(t, resp)

	body, err = ioutil.ReadAll(resp.Body)
	assert.Nil(t, err)
	assert.Contains(t, string(body), "<title>Sign Up</title>")

	// logout
	resp, err = ts.Client().Get(ts.URL + Logout)
	assert.Nil(t, err)
	assert.NotNil(t, resp)

	body, err = ioutil.ReadAll(resp.Body)
	assert.Nil(t, err)
	// we see the login page here because we get redirected
	assert.Contains(t, string(body), "<title>Login</title>")

	// reset password
	resp, err = ts.Client().Get(ts.URL + ResetPassword)
	assert.Nil(t, err)
	assert.NotNil(t, resp)

	body, err = ioutil.ReadAll(resp.Body)
	assert.Nil(t, err)
	assert.Contains(t, string(body), "<title>Reset Password</title>")

	// reset password confirm
	resp, err = ts.Client().Get(ts.URL + ResetPasswordConfirm)
	assert.Nil(t, err)
	assert.NotNil(t, resp)

	body, err = ioutil.ReadAll(resp.Body)
	assert.Nil(t, err)
	assert.Contains(t, string(body), "<title>Reset Password</title>")

	// secured pages

	// profile
	resp, err = ts.Client().Get(ts.URL + Profile)
	assert.Nil(t, err)
	assert.NotNil(t, resp)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)

	// edit
	resp, err = ts.Client().Get(ts.URL + Edit)
	assert.Nil(t, err)
	assert.NotNil(t, resp)
	assert.Equal(t, http.StatusForbidden, resp.StatusCode)
}

func TestNewServe_SimpleFlow(t *testing.T) {
	db := newMemoryDB()
	initErr := db.Init()
	assert.Nil(t, initErr)

	ses := newMemorySession()
	initErr = ses.Init()
	assert.Nil(t, initErr)

	srv := server{
		domain:  "localhost",
		mux:     http.NewServeMux(),
		storage: db,
		session: ses,
	}

	srv.routes()

	ts := httptest.NewUnstartedServer(srv.mux)
	ts.Start()
	defer ts.Close()

	// register
	// port to signup
	client := ts.Client()

	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return errors.New("do not follow redirect")
	}

	resp, err := client.
		Post(ts.URL+SignUp, "application/x-www-form-urlencoded", bytes.NewReader([]byte(("email=email&password=password"))))
	assert.NotNil(t, err)
	assert.Equal(t, "Post \"/edit\": do not follow redirect", err.Error())
	assert.NotNil(t, resp)
	cookie := resp.Header.Get("Set-Cookie")
	assert.NotNil(t, cookie)
	assert.Equal(t, http.StatusSeeOther, resp.StatusCode)

	// todo issue with setting cookie for follow up requests

	// edit

	// view profile

	// logout

	// login

	// logout

	// try view profile
}
