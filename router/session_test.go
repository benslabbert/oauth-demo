package router

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSession(t *testing.T) {
	sessionID := "id"

	session := newMemorySession()
	err := session.Init()

	assert.Nil(t, err)
	assert.NotNil(t, session)

	user, err := session.Get(sessionID)
	assert.NotNil(t, err)
	assert.Equal(t, SessionNotFound, err)
	assert.Nil(t, user)

	err = session.Remove(sessionID)
	assert.Nil(t, err)

	err = session.Set(sessionID, &User{ID: sessionID})
	assert.Nil(t, err)

	user, err = session.Get(sessionID)
	assert.Nil(t, err)
	assert.NotNil(t, user)
	assert.Equal(t, sessionID, user.ID)

	err = session.Remove(sessionID)
	assert.Nil(t, err)

	user, err = session.Get(sessionID)
	assert.NotNil(t, err)
	assert.Equal(t, SessionNotFound, err)
	assert.Nil(t, user)
}
