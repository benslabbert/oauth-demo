package router

import (
	"errors"
	"sync"
)

var SessionNotFound = errors.New("user not found")

type session interface {
	Init() error
	Close() error

	Get(id string) (*User, error)
	Set(id string, user *User) error
	Remove(id string) error
}

type memorySession struct {
	session    map[string]*User
	sessionMtx sync.Mutex
}

func (m *memorySession) Close() error {
	m.sessionMtx.Lock()
	return nil
}

func (m *memorySession) Get(id string) (*User, error) {
	m.sessionMtx.Lock()
	defer m.sessionMtx.Unlock()

	user, ok := m.session[id]
	if !ok {
		return nil, SessionNotFound
	}

	return user, nil
}

func (m *memorySession) Set(id string, user *User) error {
	m.sessionMtx.Lock()
	defer m.sessionMtx.Unlock()

	m.session[id] = user
	return nil
}

func (m *memorySession) Remove(id string) error {
	m.sessionMtx.Lock()
	defer m.sessionMtx.Unlock()

	delete(m.session, id)
	return nil
}

func newMemorySession() *memorySession {
	return &memorySession{}
}

func (m *memorySession) Init() error {
	m.session = make(map[string]*User)
	m.sessionMtx = sync.Mutex{}
	return nil
}
