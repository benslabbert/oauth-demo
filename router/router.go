package router

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/oauth2"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

type server struct {
	domain  string
	mux     *http.ServeMux
	gAuth   *GoogleAuth
	storage storage
	session session
	mailer  mailer
}

func NewServer(oauthCredentials []byte, emailApiKey, domain string) (*http.Server, error) {
	auth, err := NewGoogleAuth(oauthCredentials)
	if err != nil {
		return nil, err
	}

	memoryDB := newMemoryDB()
	err = memoryDB.Init()
	if err != nil {
		log.Fatal(err)
	}

	session := newMemorySession()
	err = session.Init()
	if err != nil {
		log.Fatal(err)
	}

	s := server{
		domain:  domain,
		mux:     http.NewServeMux(),
		gAuth:   auth,
		storage: memoryDB,
		session: session,
		mailer:  newSendGridMailer(emailApiKey),
	}

	s.routes()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	return &http.Server{
		Addr:         fmt.Sprintf("0.0.0.0:%s", port),
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      s.mux,
	}, nil
}

var NotAuthenticated = errors.New("user not authenticated")

// URL routes
const Root = "/"
const Oauth = "/oauth"
const SignUp = "/signup"
const OauthRedirect = "/redirect/google/auth"
const Profile = "/profile"
const Edit = "/edit"
const Logout = "/logout"
const ResetPassword = "/reset_password"
const ResetPasswordConfirm = "/reset_password/confirm"

func (s *server) routes() {
	// public
	s.mux.HandleFunc(Root, s.login)
	s.mux.HandleFunc(SignUp, s.signUp)
	s.mux.HandleFunc(Oauth, s.oauth)
	s.mux.HandleFunc(OauthRedirect, s.googleOAuth)
	s.mux.HandleFunc(Logout, s.logout)
	s.mux.HandleFunc(ResetPassword, s.resetPassword)
	s.mux.HandleFunc(ResetPasswordConfirm, s.resetPasswordConfirm)

	// secured
	s.mux.HandleFunc(Profile, s.profile)
	s.mux.HandleFunc(Edit, s.edit)
}

func (s *server) signUpGet(w http.ResponseWriter, r *http.Request) {
	_, err := s.userAuthenticated(r)
	if err == nil {
		redirect(w, r, Profile)
		return
	}

	data := &SignUpTmplData{
		LoginPath:  Root,
		OauthPath:  Oauth,
		SignUpPath: SignUp,
	}

	_ = SignUpTmpl.Execute(w, data)
}

func (s *server) signUpPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		badRequest(w, "unable to read request", err)
		return
	}

	values := r.Form
	email := values.Get("email")
	password := values.Get("password")

	_, err = s.storage.FindByEmail(email)

	if UserAlreadyExists == err {
		badRequest(w, "Username unavailable", err)
		return
	}

	// create new user
	user, err := s.storage.Create(NewUser(email, password))
	if err == nil {
		s.setupNewUser(w, r, user)
		return
	}

	if UserAlreadyExists == err {
		badRequest(w, "Username unavailable", err)
		return
	}

	internalError(w, err)
}

func (s *server) signUp(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		s.signUpGet(w, r)
	case http.MethodPost:
		s.signUpPost(w, r)
	default:
		methodNotAllowed(w)
	}
}

func (s *server) loginGet(w http.ResponseWriter, r *http.Request) {
	_, err := s.userAuthenticated(r)
	if err == nil {
		redirect(w, r, Profile)
		return
	}

	data := &LoginTmplData{
		LoginPath:         Root,
		OauthPath:         Oauth,
		SignUpPath:        SignUp,
		ResetPasswordPath: ResetPassword,
	}

	_ = LoginTmpl.Execute(w, data)
}

func (s *server) loginPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		badRequest(w, "unable to read request", err)
		return
	}

	values := r.Form
	email := values.Get("email")
	password := values.Get("password")

	user, err := s.storage.FindByEmail(email)

	if err == nil {
		if user.Password != password {
			badRequest(w, "invalid credentials", err)
			return
		}

		s.loginUser(w, r, user)
		return
	}

	switch err {
	case UserNotFound:
		notFound(w, "username not found", err)
	default:
		internalError(w, err)
	}
}

func (s *server) login(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		s.loginGet(w, r)
	case http.MethodPost:
		s.loginPost(w, r)
	default:
		methodNotAllowed(w)
	}
}

func (s *server) oauth(w http.ResponseWriter, r *http.Request) {
	redirect(w, r, s.gAuth.AuthURL())
}

func (s *server) googleOAuth(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	token, err := s.gAuth.config.Exchange(context.Background(), code, oauth2.ApprovalForce, oauth2.AccessTypeOffline)
	if err != nil {
		internalError(w, err)
		return
	}

	get, err := s.gAuth.config.Client(context.Background(), token).Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		internalError(w, err)
		return
	}

	body, err := ioutil.ReadAll(get.Body)
	if err != nil {
		internalError(w, err)
		return
	}

	type oauthUser struct {
		Sub           string `json:"sub"`
		Email         string `json:"email"`
		EmailVerified bool   `json:"email_verified"`
	}

	// now we have the email
	// create account and navigate to the user profile info
	u := new(oauthUser)
	err = json.Unmarshal(body, u)
	if err != nil {
		internalError(w, err)
		return
	}

	if !u.EmailVerified {
		badRequest(w, "Your email address is not verified, please confirm your email with Google", nil)
		return
	}

	user, err := s.storage.FindByEmail(u.Email)

	if err == nil {
		// we have an existing user, log them in
		s.loginUser(w, r, user)
		return
	}

	if UserNotFound == err {
		// create a new user
		user, err = s.storage.Create(NewOauthUser(u.Email, u.Sub))
		switch err {
		case nil:
			s.setupNewUser(w, r, user)
		case UserAlreadyExists:
			badRequest(w, "Username/Email not available", err)
		default:
			internalError(w, err)
		}

		return
	}

	internalError(w, err)
}

func (s *server) logout(w http.ResponseWriter, r *http.Request) {
	defer redirect(w, r, Root)

	cookie, err := r.Cookie(cookieName)
	if err != nil {
		return
	}

	userID, err := VerifyCookie(cookie)
	if err != nil {
		return
	}

	err = s.session.Remove(userID)
	if err != nil {
		// fail silently
		log.Println(err)
	}
}

func (s *server) userAuthenticated(r *http.Request) (*User, error) {
	cookie, err := r.Cookie(cookieName)
	if err != nil {
		return nil, err
	}

	userID, err := VerifyCookie(cookie)
	if err != nil {
		return nil, err
	}

	user, err := s.session.Get(userID)
	if err != nil {
		return nil, NotAuthenticated
	}

	return user, nil
}

func (s *server) edit(w http.ResponseWriter, r *http.Request) {
	user, err := s.userAuthenticated(r)
	if err != nil {
		forbidden(w, err)
		return
	}

	switch r.Method {
	case http.MethodGet:
		s.editGet(w, r, user)
	case http.MethodPost:
		s.editPost(w, r, user)
	default:
		methodNotAllowed(w)
	}
}

func (s *server) editGet(w http.ResponseWriter, r *http.Request, user *User) {
	data := &EditTmplData{
		User:        user,
		ProfilePath: Profile,
		SubmitPath:  Edit,
	}

	_ = EditTmpl.Execute(w, data)
}

func (s *server) editPost(w http.ResponseWriter, r *http.Request, user *User) {
	err := r.ParseForm()
	if err != nil {
		badRequest(w, "failed to read request", err)
		return
	}

	values := r.Form

	user.Name = values.Get("username")
	user.Address = values.Get("address")
	user.Telephone = values.Get("telephone")

	user, err = s.storage.Update(user)
	if err != nil {
		internalError(w, err)
		return
	}

	// update session
	err = s.session.Set(user.ID, user)
	if err != nil {
		internalError(w, err)
		return
	}

	redirect(w, r, Profile)
}

func (s *server) profile(w http.ResponseWriter, r *http.Request) {
	user, err := s.userAuthenticated(r)
	if err != nil {
		forbidden(w, err)
		return
	}

	data := &ProfileTmplData{
		User:       user,
		EditPath:   Edit,
		LogoutPath: Logout,
	}

	_ = ProfileTmpl.Execute(w, data)
}

func (s *server) newSession(w http.ResponseWriter, user *User) {
	cookie, err := NewCookie(user, s.domain)
	if err != nil {
		internalError(w, err)
		return
	}

	http.SetCookie(w, cookie)

	err = s.session.Set(user.ID, user)
	if err != nil {
		internalError(w, err)
	}
}

func (s *server) setupNewUser(w http.ResponseWriter, r *http.Request, user *User) {
	s.newSession(w, user)
	redirect(w, r, Edit)
}

func (s *server) loginUser(w http.ResponseWriter, r *http.Request, user *User) {
	s.newSession(w, user)
	redirect(w, r, Profile)
}

func (s *server) resetPasswordConfirmGet(w http.ResponseWriter, r *http.Request) {
	// user has followed the link to get reset password
	resetCode := r.URL.Query().Get("code")

	data := &ResetPasswordTmplData{
		LoginPath:                Root,
		ResetPasswordConfirmPath: ResetPasswordConfirm,
		Code:                     resetCode,
	}

	_ = ResetPasswordTmpl.Execute(w, data)
}

func (s *server) resetPasswordConfirmPost(w http.ResponseWriter, r *http.Request) {
	// user is submitting a new password
	err := r.ParseForm()
	if err != nil {
		badRequest(w, "unable to read request", err)
		return
	}

	code := r.Form.Get("code")
	newPassword := r.Form.Get("new_password")
	confirmPassword := r.Form.Get("confirm_password")

	// verify code
	user, err := s.storage.ResetPasswordTokenUser(code)
	if err != nil {
		badRequest(w, "invalid token", err)
		return
	}

	if confirmPassword != newPassword {
		badRequest(w, "passwords do not match", nil)
		return
	}

	// update password
	user.Password = newPassword
	_, err = s.storage.Update(user)
	if err != nil {
		internalError(w, err)
		return
	}

	s.storage.ResetPasswordTokenDelete(user.ID)

	redirect(w, r, Root)
}

func (s *server) resetPasswordConfirm(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		s.resetPasswordConfirmGet(w, r)
	case http.MethodPost:
		s.resetPasswordConfirmPost(w, r)
	default:
		methodNotAllowed(w)
	}
}

func (s *server) resetPasswordGet(w http.ResponseWriter, r *http.Request) {
	data := &ResetPasswordRequestEmailTmplData{
		LoginPath:         Root,
		ResetPasswordPath: ResetPassword,
	}

	_ = ResetPasswordRequestEmailTmpl.Execute(w, data)
}

func (s *server) resetPasswordPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		badRequest(w, "unable to read request", err)
		return
	}

	email := r.Form.Get("email")
	_, err = s.storage.FindByEmail(email)
	if err != nil {
		badRequest(w, "unknown email address", err)
		return
	}

	token, err := s.storage.ResetPasswordToken(email)
	if err != nil {
		// todo make better
		badRequest(w, "unknown email address", err)
		return
	}

	var baseUrl string

	if s.domain == "localhost" || s.domain == "127.0.0.1" {
		baseUrl = fmt.Sprintf("http://%s", s.domain)
	} else {
		baseUrl = fmt.Sprintf("https://%s", s.domain)
	}

	url := fmt.Sprintf("%s%s?code=%s", baseUrl, ResetPasswordConfirm, token)
	_ = s.mailer.SendPasswordReset(email, url)

	redirect(w, r, Root)
}

func (s *server) resetPassword(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		s.resetPasswordGet(w, r)
	case http.MethodPost:
		s.resetPasswordPost(w, r)
	default:
		methodNotAllowed(w)
	}
}

func redirect(w http.ResponseWriter, r *http.Request, url string) {
	http.Redirect(w, r, url, http.StatusSeeOther)
}

func sendError(w http.ResponseWriter, err *ErrorTmplData) {
	_ = ErrorTmpl.Execute(w, err)
}
