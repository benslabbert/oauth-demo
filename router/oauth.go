package router

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/people/v1"
)

type GoogleAuth struct {
	config *oauth2.Config
}

func NewGoogleAuth(credentials []byte) (*GoogleAuth, error) {
	config, err := google.ConfigFromJSON(credentials, people.UserinfoEmailScope)
	if err != nil {
		return nil, err
	}

	return &GoogleAuth{config: config}, nil
}

func (ga *GoogleAuth) AuthURL() string {
	return ga.config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
}
