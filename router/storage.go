package router

import (
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"strconv"
	"strings"
	"sync"
	"time"
)

type User struct {
	ID        string
	Name      string
	Address   string
	Telephone string
	Email     string
	Password  string
	OauthID   string
}

type PasswordResetToken struct {
	UserID     string
	Expiration time.Time
	Token      string
}

func NewPasswordResetToken(userID string) (PasswordResetToken, error) {
	r := make([]byte, 32)
	_, err := rand.Read(r)
	if err != nil {
		return PasswordResetToken{}, err
	}

	return PasswordResetToken{
		UserID:     userID,
		Expiration: time.Now().Add(15 * time.Minute),
		Token:      hex.EncodeToString(r),
	}, nil
}

func (prt PasswordResetToken) String() string {
	return fmt.Sprintf("%s:%d:%s", prt.UserID, prt.Expiration.Unix(), prt.Token)
}

func PasswordResetTokenFromString(str string) (PasswordResetToken, error) {
	if !strings.Contains(str, ":") {
		return PasswordResetToken{}, InvalidPasswordResetToken
	}

	if 2 != strings.Count(str, ":") {
		return PasswordResetToken{}, InvalidPasswordResetToken
	}

	split := strings.Split(str, ":")
	userID := split[0]
	expirationUnixStr := split[1]
	tokenStr := split[2]

	expiration, err := strconv.ParseInt(expirationUnixStr, 10, 64)
	if err != nil {
		return PasswordResetToken{}, InvalidPasswordResetToken
	}

	return PasswordResetToken{
		UserID:     userID,
		Expiration: time.Unix(expiration, 0),
		Token:      tokenStr,
	}, nil
}

func NewOauthUser(email, oauthID string) *User {
	return &User{
		ID:      oauthID,
		Email:   email,
		OauthID: oauthID,
	}
}

func NewUser(email, password string) *User {
	return &User{
		ID:    uuid.New().String(),
		Email: email,
		// todo hash this with salt/argon2
		Password: password,
	}
}

var UserNotFound = errors.New("user not found")
var UserAlreadyExists = errors.New("user for given email already exists")
var InvalidPasswordResetToken = errors.New("password reset token is no longer valid")
var ExpiredPasswordResetToken = errors.New("password reset token is expired")

type userStorage interface {
	Create(user *User) (*User, error)
	Update(user *User) (*User, error)
	FindByEmail(email string) (*User, error)
}

type resetPasswordStorage interface {
	ResetPasswordTokenDelete(userID string)
	ResetPasswordTokenUser(token string) (*User, error)
	ResetPasswordToken(email string) (string, error)
}

type storage interface {
	Init() error
	Close() error

	userStorage
	resetPasswordStorage
}

type memoryDB struct {
	userDB    map[string]*User
	userDBMtx sync.Mutex

	passwordResetDB  map[string]PasswordResetToken
	passwordResetMtx sync.Mutex
}

func (m *memoryDB) ResetPasswordTokenDelete(userID string) {
	m.passwordResetMtx.Lock()
	defer m.passwordResetMtx.Unlock()

	delete(m.passwordResetDB, userID)
}

func (m *memoryDB) ResetPasswordTokenUser(token string) (*User, error) {
	resetToken, err := PasswordResetTokenFromString(token)
	if err != nil {
		return nil, err
	}

	if time.Now().After(resetToken.Expiration) {
		return nil, ExpiredPasswordResetToken
	}

	m.passwordResetMtx.Lock()
	storedToken := m.passwordResetDB[resetToken.UserID]
	m.passwordResetMtx.Unlock()

	if resetToken.String() != storedToken.String() {
		return nil, InvalidPasswordResetToken
	}

	// find user
	m.userDBMtx.Lock()
	defer m.userDBMtx.Unlock()

	for _, user := range m.userDB {
		if user.ID == resetToken.UserID {
			return user, nil
		}
	}

	return nil, UserNotFound
}

func (m *memoryDB) ResetPasswordToken(email string) (string, error) {
	user, err := m.FindByEmail(email)
	if err != nil {
		return "", err
	}

	token, err := NewPasswordResetToken(user.ID)
	if err != nil {
		return "", err
	}

	m.passwordResetMtx.Lock()
	m.passwordResetDB[user.ID] = token
	m.passwordResetMtx.Unlock()

	return token.String(), nil
}

func newMemoryDB() *memoryDB {
	return &memoryDB{}
}

func (m *memoryDB) Init() error {
	m.userDB = make(map[string]*User)
	m.userDBMtx = sync.Mutex{}

	m.passwordResetDB = make(map[string]PasswordResetToken)
	m.passwordResetMtx = sync.Mutex{}

	return nil
}

func (m *memoryDB) Close() error {
	m.userDBMtx.Lock()
	return nil
}

func (m *memoryDB) Create(user *User) (*User, error) {
	_, err := m.FindByEmail(user.Email)
	if UserNotFound != err {
		return nil, UserAlreadyExists
	}

	m.userDBMtx.Lock()
	defer m.userDBMtx.Unlock()

	dbUser := &User{
		ID:        user.ID,
		Name:      user.Name,
		Address:   user.Address,
		Telephone: user.Telephone,
		Email:     user.Email,
		Password:  user.Password,
		OauthID:   user.OauthID,
	}
	m.userDB[user.Email] = dbUser
	return dbUser, nil
}

func (m *memoryDB) Update(user *User) (*User, error) {
	_, err := m.FindByEmail(user.Email)
	if err != nil {
		return nil, err
	}

	m.userDBMtx.Lock()
	defer m.userDBMtx.Unlock()

	// update existing user
	m.userDB[user.Email] = user
	return user, nil
}

func (m *memoryDB) FindByEmail(email string) (*User, error) {
	m.userDBMtx.Lock()
	defer m.userDBMtx.Unlock()

	u, ok := m.userDB[email]

	if !ok {
		return nil, UserNotFound
	}

	return u, nil
}
