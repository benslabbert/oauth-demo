package router

import (
	"log"
	"net/http"
)

func internalError(w http.ResponseWriter, err error) {
	if err != nil {
		log.Println(err)
	}

	w.WriteHeader(http.StatusInternalServerError)
	sendError(w, &ErrorTmplData{
		Type:      "Internal",
		Msg:       "We have run into a problem, please try again",
		LoginPath: Root,
	})
}

func badRequest(w http.ResponseWriter, msg string, err error) {
	if err != nil {
		log.Println(err)
	}

	w.WriteHeader(http.StatusBadRequest)
	sendError(w, &ErrorTmplData{
		Type:      "Bad Request",
		Msg:       msg,
		LoginPath: Root,
	})
}

func notFound(w http.ResponseWriter, msg string, err error) {
	if err != nil {
		log.Println(err)
	}

	w.WriteHeader(http.StatusNotFound)
	sendError(w, &ErrorTmplData{
		Type:      "Not Found",
		Msg:       msg,
		LoginPath: Root,
	})
}

func forbidden(w http.ResponseWriter, err error) {
	if err != nil {
		log.Println(err)
	}

	w.WriteHeader(http.StatusForbidden)
	sendError(w, &ErrorTmplData{
		Type:      "Forbidden",
		Msg:       "You must log in to access this page",
		LoginPath: Root,
	})
}

func methodNotAllowed(w http.ResponseWriter) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	sendError(w, &ErrorTmplData{
		Type:      "Method Not allowed",
		Msg:       "Unsupported HTTP method",
		LoginPath: Root,
	})
}
