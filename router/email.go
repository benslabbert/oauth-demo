package router

import (
	"fmt"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type mailer interface {
	SendPasswordReset(email, url string) error
}

type sendGridMailer struct {
	apiKey string
}

func newSendGridMailer(apiKey string) *sendGridMailer {
	return &sendGridMailer{apiKey: apiKey}
}

func (m *sendGridMailer) SendPasswordReset(email, url string) error {
	from := mail.NewEmail("Password Reset", "bluebuttonsoftware@gmail.com")
	subject := "Password Reset"
	to := mail.NewEmail("Password Reset", email)
	plainTextContent := fmt.Sprintf("Follow the link to reset your password: %s", url)
	htmlContent := fmt.Sprintf("Follow the link to reset your password: %s", url)
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(m.apiKey)
	_, err := client.Send(message)
	return err
}
