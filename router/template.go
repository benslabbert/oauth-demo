package router

import (
	"html/template"
	"log"
)

var LoginTmpl *template.Template
var ResetPasswordRequestEmailTmpl *template.Template
var ResetPasswordTmpl *template.Template
var SignUpTmpl *template.Template
var EditTmpl *template.Template
var ProfileTmpl *template.Template
var ErrorTmpl *template.Template

type LoginTmplData struct {
	LoginPath         string
	OauthPath         string
	SignUpPath        string
	ResetPasswordPath string
}

type ResetPasswordRequestEmailTmplData struct {
	LoginPath         string
	ResetPasswordPath string
}

type ResetPasswordTmplData struct {
	LoginPath                string
	ResetPasswordConfirmPath string
	Code                     string
}

type SignUpTmplData struct {
	LoginPath  string
	OauthPath  string
	SignUpPath string
}

type ProfileTmplData struct {
	User       *User
	EditPath   string
	LogoutPath string
}

type EditTmplData struct {
	User        *User
	ProfilePath string
	SubmitPath  string
}

type ErrorTmplData struct {
	Type      string
	Msg       string
	LoginPath string
}

func init() {
	log.Println("loading templates")

	var err error
	LoginTmpl, err = template.New("login").Parse(loginTmpl)
	if err != nil {
		log.Fatal(err)
	}

	ResetPasswordRequestEmailTmpl, err = template.New("reset-password").Parse(resetPasswordRequestEmailTmpl)
	if err != nil {
		log.Fatal(err)
	}

	ResetPasswordTmpl, err = template.New("reset-password").Parse(resetPasswordTmpl)
	if err != nil {
		log.Fatal(err)
	}

	SignUpTmpl, err = template.New("sign-up").Parse(signUpTmpl)
	if err != nil {
		log.Fatal(err)
	}

	ProfileTmpl, err = template.New("profile").Parse(profileTmpl)
	if err != nil {
		log.Fatal(err)
	}

	EditTmpl, err = template.New("edit").Parse(editTmpl)
	if err != nil {
		log.Fatal(err)
	}

	ErrorTmpl, err = template.New("error").Parse(errorTmpl)
	if err != nil {
		log.Fatal(err)
	}
}

// todo add an error to templates to display errors instead of redirecting the uer to an error page

const loginTmpl = `
<html lang="en">
    <head>
        <title>Login</title>
    </head>

    <body>
        <form action="{{.LoginPath}}" method="post">
            Email:<label><input type="email" name="email"></label>
            Password:<label><input type="password" name="password"></label>
            <input type="submit" value="Login">
        </form>

        <div>
            Login with <a href="{{.OauthPath}}">google</a>
        </div>

        <div>
            Or <a href="{{.SignUpPath}}">sign up</a>
        </div>

        <div>
            <a href="{{.ResetPasswordPath}}">reset password</a>
        </div>
    </body>
</html>
`

const resetPasswordRequestEmailTmpl = `
<html lang="en">
    <head>
        <title>Reset Password</title>
    </head>

    <body>
        <form action="{{.ResetPasswordPath}}" method="post">
            Email:<label><input type="email" name="email"></label>
            <input type="submit" value="Request Password Reset">
        </form>

        <div>
            Back to <a href="{{.LoginPath}}">login</a>
        </div>
    </body>
</html>
`

const resetPasswordTmpl = `
<html lang="en">
    <head>
        <title>Reset Password</title>
    </head>

    <body>
        <form action="{{.ResetPasswordConfirmPath}}" method="post">
            <input type="hidden" name="code" value="{{.Code}}">
            New Password:<label><input type="password" name="new_password"></label>
            Confirm Password:<label><input type="password" name="confirm_password"></label>
            <input type="submit" value="Reset Password">
        </form>

        <div>
            Back to <a href="{{.LoginPath}}">login</a>
        </div>
    </body>
</html>
`

const signUpTmpl = `
<html lang="en">
    <head>
        <title>Sign Up</title>
    </head>

    <body>
        <form action="{{.SignUpPath}}" method="post">
            Email:<label><input type="email" name="email"></label>
            Password:<label><input type="password" name="password"></label>
            <input type="submit" value="SignUp">
        </form>

        <div>
            SignUp with <a href="{{.OauthPath}}">google</a>
        </div>

        <div>
            Existing user? <a href="{{.LoginPath}}">login</a>
        </div>
    </body>
</html>
`

const profileTmpl = `
<html lang="en">
    <head>
        <title>Profile</title>
    </head>

    <body>
        <ul>
            <li>Name: {{.User.Name}}</li>
            <li>Address: {{.User.Address}}</li>
            <li>Telephone: {{.User.Telephone}}</li>
            <li>Email: {{.User.Email}}</li>
        </ul>

        <div>
            <a href="{{.EditPath}}">edit</a>
        </div>

        <div>
            <a href={{.LogoutPath}}>logout</a>
        </div>
    </body>
</html>
`

const editTmpl = `
<html lang="en">
    <head>
        <title>Edit User details</title>
    </head>

    <body>
        <form action="{{.SubmitPath}}" method="post">
            Name:<label><input value="{{.User.Name}}" type="text" name="username"></label>
            Address:<label><input value="{{.User.Address}}" type="text" name="address"></label>
            Telephone:<label><input value="{{.User.Telephone}}" type="text" name="telephone"></label>
            <input type="submit" value="Save">
        </form>

        <div>
            <a href={{.ProfilePath}}>cancel</a>
        </div>
    </body>
</html>
`

const errorTmpl = `
<html lang="en">
    <head>
        <title>Error: {{.Type}}</title>
    </head>

    <body>
        <div>
            <p>{{.Msg}}</p> <a href="{{.LoginPath}}">back to login</a>
        </div>
    </body>
</html>
`
