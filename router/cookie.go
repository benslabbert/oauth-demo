package router

import (
	"crypto/rand"
	"crypto/sha512"
	"encoding/base64"
	"errors"
	"fmt"
	"golang.org/x/crypto/pbkdf2"
	"hash"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var cookieSecret []byte

const cookieName = "challenge_session"

var InvalidCookie = errors.New("invalid cookie")

func init() {
	log.Println("deriving key for cookie signing")
	password := make([]byte, 64)
	_, err := rand.Read(password)
	if err != nil {
		log.Fatal(err)
	}

	cookieSecret = pbkdf2.Key(password[:32], password[32:], 4096, 64, sha512.New)
}

func NewCookie(user *User, domain string) (*http.Cookie, error) {
	expiration := time.Now().Add(30 * 24 * time.Hour)
	unixExpiration := expiration.Unix()

	signature, err := cookieSignature(user.ID, fmt.Sprintf("%d", unixExpiration))
	if err != nil {
		return nil, err
	}

	// todo get the domain from the env
	value := fmt.Sprintf("%s:%d:%s", user.ID, unixExpiration, signature)
	return &http.Cookie{Name: cookieName, Domain: domain, Value: value, Expires: expiration, Path: Root}, nil
}

func cookieSignature(userID, expires string) (string, error) {
	hasher := newHasher()
	hasher.Hash(cookieSecret[:32])
	hasher.Hash([]byte(expires))
	hasher.Hash([]byte(userID))
	hasher.Hash(cookieSecret[32:])

	final, err := hasher.Finalize()
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(final), nil
}

func VerifyCookie(cookie *http.Cookie) (string, error) {
	if cookie == nil {
		return "", InvalidCookie
	}

	if cookieName != cookie.Name {
		return "", InvalidCookie
	}

	if !strings.Contains(cookie.Value, ":") {
		return "", InvalidCookie
	}

	if 2 != strings.Count(cookie.Value, ":") {
		return "", InvalidCookie
	}

	split := strings.Split(cookie.Value, ":")
	userID := split[0]
	expiration := split[1]
	providedSignature := split[2]

	calculatedSignature, err := cookieSignature(userID, expiration)
	if err != nil {
		return "", err
	}

	if calculatedSignature != providedSignature {
		return "", InvalidCookie
	}

	parseUint, err := strconv.ParseInt(expiration, 10, 64)
	if err != nil {
		return "", err
	}

	if time.Now().After(time.Unix(parseUint, 0)) {
		return "", InvalidCookie
	}

	return userID, nil
}

type hasher struct {
	h   hash.Hash
	err error
}

func newHasher() *hasher {
	hasher := new(hasher)
	hasher.Init()
	return hasher
}

func (h *hasher) Init() {
	h.h = sha512.New()
}

func (h *hasher) Hash(b []byte) {
	if h.err != nil {
		return
	}

	_, e := h.h.Write(b)
	if e != nil {
		h.err = e
		return
	}

}

func (h *hasher) Finalize() ([]byte, error) {
	return h.h.Sum(nil), h.err
}
