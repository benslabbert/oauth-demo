# oauth-app-demo

## Running Locally

Add `PORT` to `.env` file to customise the port for local deployment.

Make sure you have [Go](http://golang.org/doc/install) version 1.12 or newer and the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) installed.

```sh
$ go build -o bin/oauth-app-demo -v .
$ heroku local
```

App should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```sh
$ heroku create
$ git push heroku master
$ heroku open
```

## Restart app

heroku restart -a APP_NAME
