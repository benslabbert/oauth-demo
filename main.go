package main

import (
	"context"
	"log"
	"net/http"
	"oauth-app-demo/m/v2/router"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/heroku/x/hmetrics/onload"
)

const EmailAPIKey = "EMAIL_API_KEY"
const GoogleOauthCredentials = "GOOGLE_OAUTH_CREDENTIALS"
const AppDomain = "APP_DOMAIN"

func main() {
	log.Println("starting server")

	emailApiKey := mustGetEnv(EmailAPIKey)
	googleOauthCredentials := mustGetEnv(GoogleOauthCredentials)
	domain := mustGetEnv(AppDomain)

	srv, err := router.NewServer([]byte(googleOauthCredentials), emailApiKey, domain)
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		if e := srv.ListenAndServe(); e != nil {
			if e != http.ErrServerClosed {
				log.Fatalf("Failed to serve: %v", e)
			}
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	// Block until signal is received
	sig := <-c

	log.Printf("Stopping the server. OS Signal: %v", sig)

	// Create a deadline to wait for while we shutdown
	ctx, cancel := context.WithTimeout(context.Background(), 250*time.Millisecond)
	defer cancel()

	go func() {
		if err := srv.Shutdown(ctx); err != nil {
			log.Println(err)
		}
	}()

	log.Println("shutting down server")
	<-ctx.Done()

	log.Println("exiting")
}

func mustGetEnv(env string) string {
	val := os.Getenv(env)
	if val == "" {
		log.Fatalf("%s env variable not present", env)
	}

	return val
}
