module oauth-app-demo/m/v2

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/heroku/x v0.0.0-20171004170240-705849e307dd
	github.com/manucorporat/sse v0.0.0-20150604091100-c142f0f1baea // indirect
	github.com/mattn/go-colorable v0.0.0-20150625154642-40e4aedc8fab // indirect
	github.com/mattn/go-isatty v0.0.0-20150814002629-7fcbc72f853b // indirect
	github.com/sendgrid/rest v2.6.0+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.6.0+incompatible
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/api v0.29.0
	gopkg.in/bluesuncorp/validator.v5 v5.9.1 // indirect
)
